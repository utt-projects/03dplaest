



from django.urls import path

from api import views

app_name = "api"

urlpatterns = [
    path('v2/list/grantgoal/', views.GrantGoalListAPIView.as_view(), name="api_list_grantgoal"),
    path('v2/detail/grantgoal/<int:pk>/', views.GrantGoalDetailAPIView.as_view(), name="api_detail_grantgoal"),
    path('v2/create/grantgoal/', views.GrantGoalCreateAPIView.as_view(), name="api_create_grantgoal"),

    ### URL'S CLIENT ####

    path('client/list/grantgoal/', views.ListGrantGoalClient.as_view(), name="client_list"),
    path('client/create/grantgoal/', views.CreateGrantGoalClient.as_view(), name="api_create_client")
]