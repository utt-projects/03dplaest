from django.shortcuts import render

from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import GrantGoal
from .forms import GrantGoalForm, UpdateGrantGoalForm

# Create your views here.


# # # #   G R A N T G O A L  C R U D  # # # # # 

## CREATE GRANT GOAL
class CreateGrantGoal(LoginRequiredMixin, generic.CreateView):
    template_name = "core/create.html"
    model = GrantGoal
    form_class = GrantGoalForm
    success_url = reverse_lazy('core:list_grantgoal')
    login_url = reverse_lazy("home:login")



## RETRIEVE GRANT GOAL
# LIST GRANT GOAL
class ListGrantGoal(LoginRequiredMixin, generic.ListView):
    template_name = "core/list.html"
    model = GrantGoal #queryset = GrantGoal.objects.filter(status=True)
    login_url = reverse_lazy("home:login")


# DETAIL GRANT GOAL
class DetailGrantGoal(LoginRequiredMixin, generic.View):
    template_name = "core/detail.html"
    context = {}
    login_url = reverse_lazy("home:login")

    def get(self, request, pk):
        grantgoal = GrantGoal.objects.get(id=pk)
        self.context = {
            "grantgoal": grantgoal
        }
        return render(request, self.template_name, self.context)


## UPDATE GRANT GOAL
class UpdateGrantGoal(LoginRequiredMixin, generic.UpdateView):
    template_name = "core/update.html"
    model = GrantGoal
    form_class = UpdateGrantGoalForm
    success_url = reverse_lazy('core:list_grantgoal')
    login_url = reverse_lazy("home:login")



## DELETE GRANT GOAL
class DeleteGrantGoal(LoginRequiredMixin, generic.DeleteView):
    template_name = "core/delete.html"
    model = GrantGoal
    success_url = reverse_lazy("core:list_grantgoal")
    login_url = reverse_lazy("home:login")