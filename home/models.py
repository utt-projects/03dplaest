from django.db import models


from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, default="Generic User Profile", blank=True, null=True)
    bio = models.CharField(max_length=256, default="I Love This APP", null=True, blank=True)
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.user.first_name
    

@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
        Profile.name = f"{instance.first_name} {instance.last_name }"
    instance.profile.save()


