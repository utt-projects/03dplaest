from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse

from .forms import LoginForm, SignUpForm, ProfileForm
from .models import Profile
# Create your views here.


class Index(generic.View):
    template_name = "home/index.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Ray Parra"
        }
        return render(request, self.template_name, self.context)


class LoginView(generic.View):
    template_name = "home/login.html"
    context = {}
    user = ""
    password = ""
    form_class = LoginForm
    
    def get(self, request):
        self.form_class = LoginForm()
        self.context = {
            "form": self.form_class
        }
        return render(request, self.template_name, self.context)
    
    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")
        else:
            return redirect("/login/")


def logoutView(request):
    logout(request)
    return render(request, "home/index.html", {})



class SignUp(generic.CreateView):
    template_name = "home/signup.html"
    form_class = SignUpForm
    success_url = reverse_lazy("home:index")

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get('username')
        password1 = form.cleaned_data.get('password1')
        user = authenticate(username=username, password=password1)
        login(self.request, user)
        return redirect('/')



class ProfileView(LoginRequiredMixin, generic.View):
    template_name = "home/profile_view.html"
    context = {}
    login_url = "/login/"

    def get(self, request, pk):
        self.context = {
            "profile": Profile.objects.get(id=pk)
        }
        return render(request, self.template_name, self.context)



class UpdateProfile(LoginRequiredMixin, generic.UpdateView):
    template_name = "home/update_profile.html"
    model = Profile
    form_class = ProfileForm
    success_url = reverse_lazy("home:profile_view")
    login_url = "/login/"

    def get_success_url(self):
        pk = self.kwargs["pk"]
        return reverse("home:profile_view", kwargs={"pk": pk})